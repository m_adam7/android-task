package com.muhamad.androidtasksoftxpert.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.muhamad.androidtasksoftxpert.interfaces.CallBackInterface;
import com.muhamad.androidtasksoftxpert.model.ItemModel;
import com.muhamad.androidtasksoftxpert.repository.ItemsRepository;

import java.util.List;

public class ItemViewModel extends ViewModel {

    private ItemsRepository repository = new ItemsRepository();

    private MutableLiveData<List<ItemModel>> listMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<String> errorMsg = new MutableLiveData<>();

    public MutableLiveData<List<ItemModel>> getListMutableLiveData() {
        return listMutableLiveData;
    }

    public MutableLiveData<String> getErrorMsg() {
        return errorMsg;
    }

    public void getItems(int number_page){

        repository.getDataFromServer(number_page, new CallBackInterface() {
            @Override
            public void onSuccess(List<ItemModel> list) {
                listMutableLiveData.postValue(list);
            }

            @Override
            public void onFailed(String msgError) {
                errorMsg.postValue(msgError);
            }
        });

    }
}
