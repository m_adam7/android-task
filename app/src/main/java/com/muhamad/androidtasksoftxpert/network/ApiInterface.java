package com.muhamad.androidtasksoftxpert.network;


import com.muhamad.androidtasksoftxpert.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers({"Accept: application/json"})
    @GET("cars")
    Call<ResponseModel> getCarsData(@Query("page") int page_number);

}
