package com.muhamad.androidtasksoftxpert.interfaces;

import com.muhamad.androidtasksoftxpert.model.ItemModel;

import java.util.List;

public interface CallBackInterface {

    void onSuccess(List<ItemModel> list);

    void onFailed(String msgError);
}
