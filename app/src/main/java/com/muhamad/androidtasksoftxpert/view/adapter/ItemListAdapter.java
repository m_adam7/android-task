package com.muhamad.androidtasksoftxpert.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.muhamad.androidtasksoftxpert.R;
import com.muhamad.androidtasksoftxpert.databinding.ItemListBinding;
import com.muhamad.androidtasksoftxpert.model.ItemModel;

import java.util.List;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ListViewHolder> {

    private List<ItemModel> itemModels;
    private LayoutInflater layoutInflater;
    private Activity activity;

    public ItemListAdapter(Activity activity, List<ItemModel> list) {
        layoutInflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.itemModels = list;
    }

    @NonNull
    @Override
    public ItemListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.item_list,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemListAdapter.ListViewHolder holder, int position) {
        ItemModel model = itemModels.get(position);

        Glide.with(activity).load(model.getImageUrl()).into(holder.itemListBinding.itemImage);
        holder.itemListBinding.itemBrand.setText(model.getBrand());
        if (model.isIsUsed()) {
            holder.itemListBinding.itemStatus.setText("Used Car");
        }else{
            holder.itemListBinding.itemStatus.setText("New Car");
        }
        holder.itemListBinding.itemDate.setText(model.getConstractionYear());
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    static class ListViewHolder extends RecyclerView.ViewHolder {

        ItemListBinding itemListBinding;

        ListViewHolder(@NonNull ItemListBinding itemView) {
            super(itemView.getRoot());
            this.itemListBinding = itemView;
        }
    }
}
