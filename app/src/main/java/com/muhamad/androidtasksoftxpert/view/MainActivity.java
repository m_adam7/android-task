package com.muhamad.androidtasksoftxpert.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.widget.Toast;

import com.muhamad.androidtasksoftxpert.R;
import com.muhamad.androidtasksoftxpert.databinding.ActivityMainBinding;
import com.muhamad.androidtasksoftxpert.model.ItemModel;
import com.muhamad.androidtasksoftxpert.view.adapter.ItemListAdapter;
import com.muhamad.androidtasksoftxpert.viewmodel.ItemViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ActivityMainBinding mainBinding;
    private ItemViewModel viewModel;
    private List<ItemModel> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        init();
        viewModel.getItems(1);
    }

    private void init() {

        viewModel = new ViewModelProvider(this).get(ItemViewModel.class);

        ItemListAdapter adapter = new ItemListAdapter(this, list);
        mainBinding.recycleView.setAdapter(adapter);

        mainBinding.swipeContainer.setOnRefreshListener(this);

        viewModel.getListMutableLiveData().observe(this, list -> {
            if (!list.isEmpty()){
                if (mainBinding.swipeContainer.isRefreshing()){
                    mainBinding.swipeContainer.setRefreshing(false);
                }
                this.list.clear();
                this.list.addAll(list);
                adapter.notifyDataSetChanged();
            }
        });

        viewModel.getErrorMsg().observe(this, s -> {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        });

    }

    @Override
    public void onRefresh() {
        viewModel.getItems(1);
    }
}
