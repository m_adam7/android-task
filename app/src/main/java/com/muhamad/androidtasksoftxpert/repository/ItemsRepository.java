package com.muhamad.androidtasksoftxpert.repository;

import androidx.annotation.NonNull;

import com.muhamad.androidtasksoftxpert.interfaces.CallBackInterface;
import com.muhamad.androidtasksoftxpert.model.ResponseModel;
import com.muhamad.androidtasksoftxpert.network.ApiClient;
import com.muhamad.androidtasksoftxpert.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemsRepository {

    public void getDataFromServer(int page_number, CallBackInterface backInterface){

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseModel> responseCall = apiService.getCarsData(page_number);

        responseCall.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<ResponseModel> call,@NonNull Response<ResponseModel> response) {
                if (response.body() != null){
                    if (response.body().getStatus() == 1){
                        backInterface.onSuccess(response.body().getData());
                    } else {
                        backInterface.onFailed("Error Happened!!, try Again");
                    }
                } else {
                    backInterface.onFailed("Error Happened!!, try Again");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseModel> call,@NonNull Throwable t) {
                backInterface.onFailed(t.getMessage());
            }
        });

    }

}
