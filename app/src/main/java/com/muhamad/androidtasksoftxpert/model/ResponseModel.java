package com.muhamad.androidtasksoftxpert.model;

import java.util.List;

public class ResponseModel {

    private int status;
    private List<ItemModel> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ItemModel> getData() {
        return data;
    }

    public void setData(List<ItemModel> data) {
        this.data = data;
    }

}
